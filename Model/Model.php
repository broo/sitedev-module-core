<?php

namespace Core\Sitedev\Module\Model;

abstract class Model extends \App_Model
{
    const DRAFT     = 1;
    const PUBLISHED = 2;
    const ARCHIVE   = 3;
    const REJECTED  = 4;

    /**
     * @var array[array]
     */
    protected static $_statuses;

    public function __construct()
    {
        parent::__construct('model');

        $this->addPrimaryKey('integer');
//         $this->addAttr('object_type_id', 'integer');
        $this->addAttr('status_id', 'integer');
        $this->addAttr('name', 'string');
        $this->addAttr('title', 'string');
        $this->addAttr('publish_date', 'integer');
        $this->addAttr('creation_time', 'integer');
    }

    /**
     * @return array[array]
     */
    public static function getStatuses()
    {
        if (!isset(static::$_statuses)) {
            static::$_statuses = array(
                static::DRAFT     => array('title' => 'черновик'),
                static::PUBLISHED => array('title' => 'опубликовано'),
                static::ARCHIVE   => array('title' => 'в архиве'),
                static::REJECTED  => array('title' => 'забраковано')
            );

            foreach (array_keys(static::$_statuses) as $id) {
                static::$_statuses[$id]['id'] = $id;
            }
        }

        return static::$_statuses;
    }

    protected function _checkAndFillName()
    {
        if (!$this->name) {
            $this->name = \App_Cms_Ext_File::normalizeName($this->title);
        }
    }

    public function create()
    {
//         if (!$this->_objectTypeId) {
//             $this->_objectTypeId = static::OBJECT_TYPE_ID;
//         }

        $this->_checkAndFillName();
        return parent::create();
    }

    public function update()
    {
        $this->_checkAndFillName();
        return parent::update();
    }

    public static function getList($_where = null, $_params = null)
    {
        $where = empty($_where) ? array() : $_where;
//         $where['object_type_id'] = static::OBJECT_TYPE_ID;

        $params = empty($_params) ? array() : $_params;
        if (!isset($params['order'])) {
            $params['order'] = 'publish_date DESC';
        }

        return parent::getList($where, $params);
    }

    public function fillWithData(array $_data)
    {
        $data = $_data;

        if (!empty($data['publish_date'])) {
            $this->publishDate = \Ext_Date::getDate($data['publish_date']);
            unset($data['publish_date']);
        }

        parent::fillWithData($data);
    }

    public function getBackOfficeXml($_xml = array(), $_attrs = array())
    {
        $attrs = $_attrs;

        if (!isset($attrs['is_published'])) {
            $attrs['is_published'] = $this->statusId == static::PUBLISHED;
        }

        return parent::getBackOfficeXml($_xml, $attrs);
    }

    /**
     * @return \App_Cms_Back_Office_NavFilter
     */
    public static function getCmsNavFilter()
    {
        $filter = new \App_Cms_Back_Office_NavFilter(get_called_class());


        // Дата

        $filter->addElement(new \App_Cms_Back_Office_NavFilter_Element_Date(
            'publish_date',
            'integer',
            'Дата публикации'
        ));


        // Заголовок

        $filter->addElement(
            new \App_Cms_Back_Office_NavFilter_Element('title', 'Название')
        );


        // Статус

        $statuses = static::getStatuses();

        if (count($statuses) > 0) {
            $el = new \App_Cms_Back_Office_NavFilter_Element_Multiple(
                'status_id',
                'Статус'
            );

            foreach ($statuses as $item) {
                $el->addOption($item['id'], $item['title']);
            }

            $filter->addElement($el);
        }


        $filter->run();
        return $filter;
    }
}
