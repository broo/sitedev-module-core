<?php

$wd = dirname(__FILE__) . '/';
require_once $wd . '../../../../core/library/libs.php';

initSettings();
$nl = PHP_EOL;

$sql = str_replace(
    '{$prfx}',
    Ext_Db::get()->getPrefix(),
    file_get_contents($wd . 'init.sql')
);

$queries = explode(';', $sql);

foreach ($queries as $query) {
    if (trim($query)) {
        Ext_Db::get()->execute($query);
    }
}

echo 'Готово';
echo $nl . $nl;
