SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';


DROP TABLE IF EXISTS `{$prfx}module` ;

CREATE  TABLE IF NOT EXISTS `{$prfx}module` (
  `{$prfx}module_id` INT UNSIGNED NOT NULL AUTO_INCREMENT ,
--  `object_type_id` TINYINT UNSIGNED NOT NULL ,
  `status_id` TINYINT NOT NULL ,
  `name` VARCHAR(255) NULL ,
  `title` VARCHAR(255) NOT NULL ,
  `publish_date` INT NULL ,
  `creation_time` INT NOT NULL ,
  PRIMARY KEY (`{$prfx}module_id`) ,
--  INDEX `fk_{$prfx}module_object_type_id_idx` (`object_type_id` ASC) ,
--  CONSTRAINT `fk_{$prfx}module_object_type_id`
--    FOREIGN KEY (`object_type_id` )
--    REFERENCES `{$prfx}object_type` (`{$prfx}object_type_id` )
--    ON DELETE CASCADE
--    ON UPDATE CASCADE
) ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
